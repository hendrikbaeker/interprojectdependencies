import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Generator {

    public static void main(String[] args) throws IOException {
        final Path dirPath = Path.of(args[0]);

        if(!Files.exists(dirPath)){
            Files.createDirectory(dirPath);
        }

        final Path filePath1 = Path.of(args[0], args[1]);
        final boolean exists = Files.exists(filePath1);
        if(!exists){
            Files.createFile(filePath1);

        }

    }


}
