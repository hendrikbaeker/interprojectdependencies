import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.OutputFile

class GeneratorExecutor extends JavaExec {

    private final String resourcePath = "${project.buildDir}/generated/resources"

    @OutputFile
    public File outputFile1

    GeneratorExecutor() {
        outputFile1 = new File(resourcePath, "filename1")
        main = 'Generator'
        classpath = project.sourceSets.main.runtimeClasspath
        args resourcePath, "filename1", "filename2"
    }
}
